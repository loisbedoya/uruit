from .models import *
from .forms import *
from django.views.generic import UpdateView, CreateView, DetailView, ListView
from django.core.urlresolvers import reverse_lazy
from django.db.models import Count
import logging

logger = logging.getLogger("info_logger")

class Begin(CreateView):
    model=Game
    template_name='begin.html'
    form_class=BeginForm

    def dispatch(self, request, *args, **kwargs):
        return super(Begin, self).dispatch(request,*args,**kwargs)

    def get_success_url(self):
        return reverse_lazy( 'player1', kwargs={'pk': self.object.pk})

    def form_valid(self, form):
        try:            
            self.object=form.save()
            self.success_url=self.get_success_url()
            logger.info('The game has saved')
            return super(Begin, self).form_valid(form)
        except IntegrityError:
            logger.error('Error creating the game')
            pass
    
    def form_invalid(self, form):
        logger.warning("Invalid form")
        return super(Begin, self).form_invalid(form)

class Player1(UpdateView):
    template_name = 'play.html'
    model=Game
    form_class=Player1Form

    def get_context_data(self, **kwargs):
        context = super(Player1, self).get_context_data(**kwargs)
        context['player']= self.object.player1
        context['round']= self.object.score_player1 + self.object.score_player2 + 1
        context['score1']= self.object.score_player1 
        context['score2']= self.object.score_player2 
        return context

    def get_success_url(self):
        return reverse_lazy( 'player2', kwargs={'pk': self.object.pk})

    def form_valid(self, form):
        try:  
            self.object=form.save()
            self.success_url=self.get_success_url()
            logger.info('The game saved the move of player 1')
            return super(Player1, self).form_valid(form)
        except Exception:
            logger.error('Error updating the game in the move of player1')
            pass        
    
    def form_invalid(self, form):
        logger.warning("Invalid form")
        return super(Player1, self).form_invalid(form)
        
class Player2(UpdateView):
    template_name = 'play.html'
    model=Game
    form_class=Player2Form

    def get_context_data(self, **kwargs):
        context = super(Player2, self).get_context_data(**kwargs)
        context['player']= self.object.player2
        context['round']= self.object.score_player1 + self.object.score_player2 + 1
        context['score1']= self.object.score_player1 
        context['score2']= self.object.score_player2 
        return context

    def get_success_url(self):
        if self.object.score_player1 == 3:
            self.object.winner = self.object.player1
            self.object.save()
            return reverse_lazy( 'win', kwargs={'pk': self.object.pk})
        elif self.object.score_player2 == 3:
            self.object.winner = self.object.player2
            self.object.save()
            return reverse_lazy( 'win', kwargs={'pk': self.object.pk})
        else:
            return reverse_lazy( 'player1', kwargs={'pk': self.object.pk})

    def form_valid(self, form):
        try:
            self.object=form.save()
            self.object.score()
            self.success_url=self.get_success_url()
            logger.info('The game saved the move of player 2')
            return super(Player2, self).form_valid(form)
        except Exception:
            logger.error('Error updating the game in the move of player2')
            pass
    
    def form_invalid(self, form):
        logger.warning("Invalid form")
        return super(Player2, self).form_invalid(form)

class Win(DetailView):
    model=Game
    template_name='win.html'

    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
            context = self.get_context_data(object=self.object)
            logger.info('We have a winner')
            return self.render_to_response(context)
        except Exception:
            logger.error('Error getting the object on win view')
            pass

    def get_context_data(self, **kwargs):
        context = super(Win, self).get_context_data(**kwargs)
        context['winner']= self.object.winner
        return context

class Historical(ListView):
    model = Game
    template_name = 'list.html'

    def dispatch(self, request, *args, **kwargs):
        return super(Historical, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(Historical, self).get_context_data(**kwargs)
        context['headers'] = ["Player1","Player2","Score player 1","Score player 2","Winner","Date"]
        context['elements'] = Game.objects.exclude(winner='')
        context['type'] = 1
        return context

class Statistics(ListView):
    model = Game
    template_name = 'list.html'

    def dispatch(self, request, *args, **kwargs):
        return super(Statistics, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(Statistics, self).get_context_data(**kwargs)
        context['headers'] = ["Player","Games won"]
        context['elements'] = Game.objects.exclude(winner='').values('winner').annotate(dcount=Count('winner')).order_by('-dcount')
        return context
   