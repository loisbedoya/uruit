from django.db import models

class Game(models.Model):
    MOVES = (
        ("Paper", "Paper"),
        ("Rock", "Rock"),
        ("Scissors", "Scissors"),
    )
    player1 = models.CharField(max_length=100, verbose_name="player 1")
    player2 = models.CharField(max_length=100, verbose_name="player 2")
    last_move_player1 = models.CharField(max_length=100, choices=MOVES,verbose_name="Select move player 1")
    last_move_player2 = models.CharField(max_length=100, choices=MOVES,verbose_name="Select move player 2")
    score_player1 = models.PositiveSmallIntegerField(default=0, null=True, blank=True, verbose_name="Número de horas")
    score_player2 = models.PositiveSmallIntegerField(default=0, null=True, blank=True, verbose_name="Número de horas")
    winner = models.CharField(blank=True,max_length=100, verbose_name="winner")
    date = models.DateField(auto_now_add=True, blank=True)

    def score(self):
        if self.last_move_player1 == self.last_move_player2:
            return 0
        elif self.last_move_player1 == "Rock":
            if self.last_move_player2 == "Paper":
                self.score_player2 += 1
                self.save()
                return 2
            else:
                self.score_player1 += 1
                self.save()
                return 1

        elif self.last_move_player1 == "Paper":
            if self.last_move_player2 == "Scissors":
                self.score_player2 += 1
                self.save()
                return 2
            else:
                self.score_player1 += 1
                self.save()
                return 1

        elif self.last_move_player1 == "Scissors":
            if self.last_move_player2 == "Rock":
                self.score_player2 += 1
                self.save()
                return 2
            else:
                self.score_player1 += 1
                self.save()
                return 1

        
    