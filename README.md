# README #
Game of drones

===========
1. Install python 3.6.2

2. Install the requeriments
    pip install -r requirements.txt

3. Modify the values of secrets.json for database access.

4. For every app in folder "apps" should create a folder "migrations" and in this create a file \_\_init\_\_.py

5. Sync up the database:
    python manage.py makemigrations
    python manage.py migrate
    
6. Run the server:
    python manage.py runserver
