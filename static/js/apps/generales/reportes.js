function tipo_reporte(){

    var tipo = null;
    var tipo_grafico = $('#seleccion-tipo-grafico').val();
    if (tipo_grafico == 'barras') {
        tipo = 'bar';
    } else if (tipo_grafico == 'barras_horizontales') {
        tipo = 'horizontalBar';
    }
    return tipo;
}



function generar_reporte_barras(url_ajax, panel_grafico) {
    var tipo = tipo_reporte();
    $.ajax({
        type: "GET",
        url: url_ajax,
        dataType: 'json',
        success: function (data) {
            var barChartData = {
                labels: data.etiquetas,
                datasets: [{
                    label: "Aspirantes",
                    borderWidth: 2,
                    backgroundColor: data.colores,
                    borderColor: data.colores,
                    data: data.valores
                }]
            };
            $('#contenedor-grafico').empty();
            $('#contenedor-grafico').append('<canvas id='+panel_grafico+'></canvas>');
            var contexto = document.getElementById(panel_grafico).getContext('2d');
            var titulo_ejeY = '';
            var titulo_ejeX = '';
            if (tipo == 'bar') {
                titulo_ejeY = data.ejeY
                titulo_ejeX = data.ejeX;
            } else if (tipo == 'horizontalBar'){
                // Con las barras horizontales se deben intercambiar las etiquetas de los ejes
                titulo_ejeY = data.ejeX;
                titulo_ejeX = data.ejeY;
            }
            var barChart = new Chart(contexto, {
                type: tipo,
                data: barChartData,
                options: {
                    title: {
                        display: true,
                        text: 'Aspirantes por perfil'
                    },
                    legend: {
                        display: false
                    },
                    scales: {
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeX
                            }, ticks: {
                                beginAtZero: true
                            }

                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeY
                            }, ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });
        }
    });
}


function generar_reporte_barras_restriccion_fecha(url_ajax, panel_grafico, selector_fecha) {
    var tipo = tipo_reporte();
    var fecha_inicio = $(selector_fecha).data('daterangepicker').startDate.format();
    var fecha_fin = $(selector_fecha).data('daterangepicker').endDate.format();

    $.ajax({
        type: "GET",
        url: url_ajax,
        dataType: 'json',
        data: {
            'fecha_inicio': fecha_inicio,
            'fecha_fin': fecha_fin
        },
        success: function (data) {
            var barChartData = {
                labels: data.etiquetas,
                datasets: [{
                    label: "Aspirantes",
                    borderWidth: 2,
                    backgroundColor: data.colores,
                    borderColor: data.colores,
                    data: data.valores
                }]
            };
            $('#contenedor-grafico').empty();
            $('#contenedor-grafico').append('<canvas id='+panel_grafico+'></canvas>');
            var contexto = document.getElementById(panel_grafico).getContext('2d');
            var titulo_ejeY = '';
            var titulo_ejeX = '';
            if (tipo == 'bar') {
                titulo_ejeY = data.ejeY
                titulo_ejeX = data.ejeX;
            } else if (tipo == 'horizontalBar'){
                // Con las barras horizontales se deben intercambiar las etiquetas de los ejes
                titulo_ejeY = data.ejeX;
                titulo_ejeX = data.ejeY;
            }
            var barChart = new Chart(contexto, {
                type: tipo,
                data: barChartData,
                options: {
                    title: {
                        display: true,
                        text: 'Aspirantes por fecha'
                    },
                    legend: {
                        display: false
                    },
                    scales:{
                        xAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeX
                            }, ticks: {
                                beginAtZero: true
                            }
                        }],
                        yAxes: [{
                            scaleLabel: {
                                display: true,
                                labelString: titulo_ejeY
                            }, ticks: {
                                beginAtZero: true
                            }
                        }]
                    }
                }
            });

        }
    });
}


function generar_reporte(boton_actualizar, url_ajax, panel_grafico) {

    var funcion_reporte_a_usar = generar_reporte_barras;
    var tipo = null; //$('#seleccion-tipo-grafico').val();
    var tipo_grafico = $('#seleccion-tipo-grafico').val();
    if (tipo_grafico == 'barras') {
        //funcion_reporte_a_usar = generar_reporte_barras;
        tipo = 'bar'
    } else if (tipo_grafico == 'barras_horizontales') {
        //funcion_reporte_a_usar = generar_reporte_barras;
        tipo = 'horizontalBar'
    }
    $(boton_actualizar).on('click', function () {
        funcion_reporte_a_usar(url_ajax, panel_grafico, tipo);
    });
    funcion_reporte_a_usar(url_ajax, panel_grafico, tipo);
    /*
     'a[data-toggle="tab"]' son los elementos 'a' con data-toogle = 'tab'
     'shown.bs.tab' es el evento generado cuando la pestaña se muestra
     */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        funcion_reporte_a_usar(url_ajax, panel_grafico, tipo);

    });
}



function generar_reporte_fecha(boton_actualizar, url_ajax, panel_grafico, selector_fecha) {

    var funcion_reporte_a_usar = null;
    var tipo = null;
    var tipo_grafico = $('#seleccion-tipo-grafico').val();
    funcion_reporte_a_usar = generar_reporte_barras_restriccion_fecha;

    if (tipo_grafico == 'barras') {
        funcion_reporte_a_usar = generar_reporte_barras_restriccion_fecha;
        tipo = 'bar'
    } else if (tipo_grafico == 'barras_horizontales') {
        funcion_reporte_a_usar = generar_reporte_barras_restriccion_fecha;
        tipo = 'horizontalBar'
    }
    $(boton_actualizar).on('click', function () {
        funcion_reporte_a_usar(url_ajax, panel_grafico, selector_fecha, tipo);
    });
    funcion_reporte_a_usar(url_ajax, panel_grafico, selector_fecha, tipo);

    /*
     'a[data-toggle="tab"]' son los elementos 'a' con data-toogle = 'tab'
     'shown.bs.tab' es el evento generado cuando la pestaña se muestra
     */
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        funcion_reporte_a_usar(url_ajax, panel_grafico, selector_fecha, tipo);

    });
}